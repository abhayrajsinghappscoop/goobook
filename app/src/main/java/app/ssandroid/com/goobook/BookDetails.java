package app.ssandroid.com.goobook;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import android.widget.TextView;

public class BookDetails extends AppCompatActivity {

    TextView mTextViewTitle, mTextViewAuthor, mTextViewSubtitle;
    String[] bookDetails;
    //Button mTextViewPdf;
    NetworkImageView mImageView;
    ImageLoader imageLoader;
    View layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        // to go back manually and load data again
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayOptions(R.id.download);

        // get the intent from the MainActivity and get every extra passed
        Intent intent = getIntent();
        // order: id [0], title [1], subtitle [2], author [3], image [4], pdf [5]
        bookDetails = intent.getStringArrayExtra("book");


        mTextViewTitle = findViewById(R.id.TextViewTitle);
        mTextViewAuthor = findViewById(R.id.TextViewAuthor);
        //mTextViewPdf = findViewById(R.id.TextViewPdf);
        mTextViewSubtitle = findViewById(R.id.TextViewSubtitle);
        mImageView = findViewById(R.id.ImageView);
        layout = findViewById(R.id.layout_book_details);


        //Toast.makeText(this, "pressed", Toast.LENGTH_SHORT).show();

        mTextViewTitle.setText(bookDetails[1]);
        mTextViewSubtitle.setText(bookDetails[2]);
        mTextViewAuthor.setText(bookDetails[3]);
        // image setting on the imageView with the link given
        mImageView.setImageResource(R.drawable.ic_dialog_close_light);
        loadImage(bookDetails[4]);

        // PDF download option if need be

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.download, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.download) {
            //Toast.makeText(BookDetails.this, "Action clicked", Toast.LENGTH_LONG).show();
            if (!bookDetails[5].equalsIgnoreCase("0"))
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(bookDetails[5])));
            else
                Snackbar.make(layout, "No PDF available", Snackbar.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadImage(String url){
        if(url.equals("0")){
            mImageView.setDefaultImageResId(android.R.drawable.ic_menu_report_image);
            //Toast.makeText(this,"No Image found",Toast.LENGTH_LONG).show();
            return;
        }

        imageLoader = CustomVolleyRequest.getInstance(this.getApplicationContext()).getImageLoader();
        imageLoader.get(url, ImageLoader.getImageListener(mImageView, android.R.drawable.ic_menu_report_image, android.R.drawable
                        .ic_menu_report_image));
        mImageView.setImageUrl(url, imageLoader);
    }
}
